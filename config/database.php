<?php

return [
	'dbOptions' => [
		'driver' => 'pdo_mysql',
		'charset' => 'utf8',
		'host' => 'localhost',
		'port' => 3306,
		'dbname' => 'config',
		'user' => 'root',
		'password' => 'root',
	],
	'slimOptions' => [
		'settings' => [
			'displayErrorDetails' => true,
		],
	],
	'viewsOptions' => [
		'engine' => 'twig',
		'path' => '/resources/views',
		'cache' => false,
	],
];