<?php

namespace App\Config\Contract;

interface ParserInterface {

	public function parse($file);

}