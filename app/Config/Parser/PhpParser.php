<?php

namespace App\Config\Parser;

use App\Config\Contract\ParserInterface;

class PhpParser implements ParserInterface {

	public function parse($file) {

		return require $file;
	}

}